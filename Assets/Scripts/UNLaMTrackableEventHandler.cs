﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class UNLaMTrackableEventHandler : MonoBehaviour, ITrackableEventHandler {

    private int departmentID;

    TrackableBehaviour UNLaMTrackableBehaviour;
    VuMarkManager UNLaMVuMark;

    void Start()
    {
        UNLaMTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (UNLaMTrackableBehaviour != null)
        {
            UNLaMTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
        UNLaMVuMark = TrackerManager.Instance.GetStateManager().GetVuMarkManager();
    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED || newStatus == TrackableBehaviour.Status.TRACKED || newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackerFound();
        }
        else if (previousStatus == TrackableBehaviour.Status.TRACKED && newStatus == TrackableBehaviour.Status.NOT_FOUND)
        {
            OnTrackerLost();
        }
    }

    void OnTrackerFound()
    {
        foreach (var item in UNLaMVuMark.GetActiveBehaviours())
        {
            departmentID = System.Convert.ToInt32(item.VuMarkTarget.InstanceId.NumericValue);
            if (MarkerIDIsValid(departmentID))
                SetActiveMarkerObject(departmentID, true);
            else
                Debug.Log("Marker doesn't exist");
        }
    }

    void OnTrackerLost()
    {
        if (MarkerIDIsValid(departmentID))
            SetActiveMarkerObject(departmentID, false);
    }

    private void SetActiveMarkerObject(int id, bool value)
    {
        transform.GetChild(id).gameObject.SetActive(value);
    }

    private bool MarkerIDIsValid(int markerID)
    {
        return markerID < QuantityOfMarkers();
    }

    private int QuantityOfMarkers()
    {
        return transform.childCount;
    }
}
