﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarkerController : MonoBehaviour {

    public string url;
    
    private void OnMouseDown()
    {
        GoToURL(url);
    }

    private void GoToURL(string url)
    {
        Application.OpenURL(url);
    }
}
